<?php

use Illuminate\Database\Seeder;

class PruebasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pruebas')->insert([
            'descripcion' => 'CIPSA',
            'formato' => '{
    "area1": [
        {"profesion": "Químico", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Ingeniero electrónico", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Matemático", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Técnico Informático", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Ingeniero naval", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Programador de sistemas", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Ingeniero en caminos", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Ingeniero en telecomunicaciones", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Astrónomo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Técnico en electrónica", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Ingeniero de minas", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Proyectista de maquinaria", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Físico nuclear", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Ingeniero aeronáutica", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Estadístico", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Ingeniero espacial", "personal": 0, "social": 0, "economico": 0}
    ],
    "area2": [
        {"profesion": "Juez", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Diplomático", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Asesor ejecutivo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Secretario de juzgado", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Concejal", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Abogado laboratorista", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Miembro ejecutiva partido política", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Diputado", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Abogado criminalista", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Senador", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Embajador", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Funcionario de ministerios", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Político", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Abogado administrativo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Ministro", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Secretario de ayuntamiento", "personal": 0, "social": 0, "economico": 0}
    ],
    "area3": [
        {"profesion": "Médico de medicina general", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Dermatólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Enfermero", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Psiquiatra", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Neumólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Oftalmólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Practicante", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Ginecólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Fisiólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Anestesista", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Endocrino", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Radiólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Analista", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Otorrino", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "A.T.S (Ayudante Técnico Sanitario)", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Cirujano", "personal": 0, "social": 0, "economico": 0}
    ],
    "area4": [
        {"profesion": "Inspector de Policía", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Mecánico", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Albañil", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Peluquero", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Policía municipal", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Conductor de servicio público", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Bombero", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Azafato", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Cartero", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Fontanero", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Empleado del servicio de basuras", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Electricista", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Empleado de servicios - públicos", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Guarda Civil", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Esteticista", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Carpintero", "personal": 0, "social": 0, "economico": 0}
    ],
    "area5": [
        {"profesion": "Historiador", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Etnólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Auxiliar de puericultura", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Sociología", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Misionero", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Psicólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Filólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Arqueólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Antropólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Asistente social", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Lingüista", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Religioso", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Asesor familiar", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Filósofo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Orientador Psicopedagogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Sacerdote", "personal": 0, "social": 0, "economico": 0}
    ],
    "area6": [
        {"profesion": "Zoólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Bioquímico", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Ecólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Ganadero", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Botánico", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Perito agrícola", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Veterinario", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Geólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Ingeniero agrónomo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Farmacéutico", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Bacteriólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Floricultor", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Biólogo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Técnico agropecuario", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Oceanógrafo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Agricultor", "personal": 0, "social": 0, "economico": 0}
    ],
    "area7": [
        {"profesion": "Novelista", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Autor teatral", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Crítico musical", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Ensayista", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Periodista", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Crítico literario", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Corresponsal en el extranjero", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Crítico de deportes", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Crítico de cine", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Reportero", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Escritor narrativo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Académico", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Poeta", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Guionista", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Crítico de arte", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Redactor de periódico", "personal": 0, "social": 0, "economico": 0}
    ],
    "area8": [
        {"profesion": "Técnico de publicidad", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Redactor de publicidad", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Dibujante", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Diseñador de moda", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Relaciones Públicas", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Presentador de televisión", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Técnico de sonido", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Coreógrafo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Agende de publicidad", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Locutor de radio", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Delineante", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Animador de espectáculos", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Escenógrafo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Modelo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Disc-jockey", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Investigador de mercados", "personal": 0, "social": 0, "economico": 0}
    ],
    "area9": [
        {"profesion": "Cantante de ópera - coro", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Instrumentista", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Humorista", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Bailarín", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Fotógrafo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Ceramista", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Compositor musical", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Pintor artístico", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Caricatura", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor de orquesta", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Escultor", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Actor", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Decorador artístico", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Director de orquesta", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Arquitecto", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Cantante solista", "personal": 0, "social": 0, "economico": 0}
    ],
    "area10": [
        {"profesion": "Alcalde", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Director de hotel", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Programador de radio o televisión", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Jefe de Policía municipal", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Piloto de vuelo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Jefe de guardia civil", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Director de colegio", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Jefe de personal", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Jefe de Policía Nacional", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Director teatral ", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Comisario de policía", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Capitán de barco", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Gobernador", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Director de cine", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Realizador de televisión", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Controlador aéreo", "personal": 0, "social": 0, "economico": 0}
    ],
    "area11": [
        {"profesion": "Profesor de formación profesional", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor de Educación Física", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor de ciencias naturales", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor de Física", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor de Dibujo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor de Música", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor de Idiomas", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor de Educación especial", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor de Primaria/Secundaria", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor de Historia", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor  de Lenguaje", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor de Lenguas clásicas", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor de Universidad", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor de Arte", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor de Matemáticas", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Profesor de Química", "personal": 0, "social": 0, "economico": 0}
    ],
    "area12": [
        {"profesion": "Cajero", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Interventor de banco", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Productor de cine", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Director de banco", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Auxiliar administrativo", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Gerente", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Director de ventas", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Empleado de comercio", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Secretario", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Contable", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Director de empresa", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Economista", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Técnico de marketing", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Intendente comercial", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Agente inmobiliaria", "personal": 0, "social": 0, "economico": 0},
        {"profesion": "Jefe de ventas o compras", "personal": 0, "social": 0, "economico": 0}
    ]
}',
            'estado' => 'ACTIVO',
            'id_usuario_creacion' => 1,
            'creacion' => date("Y-m-d H:i:s"),
        ]);
        DB::table('pruebas')->insert([
            'descripcion' => 'PEARSON',
            'formato' => '{
    "seccion_a": [
        {
            "id": 1,
            "pregunta": "Facilmente ANSIOSO, INQUIETO (por ejemplo: preocupación injustificada del provenir, turbado por cosas insignificantes).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 2,
            "pregunta": "ACTIVO, TRABAJADOR. De ordinario ocupado en algún trabajo útil,  aún en los tiempos de ocio y de recreo. (No puede quedarse sin hacer nada, desocupado o soñando).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 3,
            "pregunta": "ARDOSO para expresar su opinión, defender sus ideas o aluna causa. (Se enardece protamente).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 4,
            "pregunta": "EMPEÑADO en la ejecución de las tareas emprendidas, y más bien estimulo que contrariado por las dificultades.",
            "valor": 0,
            "total": 0
        },
        {
            "id": 5,
            "pregunta": "SUSCEPTIBLE, IRRITABLE, se impacienta fácilmente (inclinado a sentir fuertemente una contradicción).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 6,
            "pregunta": "RESUELTO, DECIDIO. (Toma fácilmente una resolución aún en casos difíciles).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 7,
            "pregunta": "INCLINADO A REACCIONAR INTENSAMENTE (interiormente por lo menos) ante un acontencimiento dado; tendencia a responder vivamente, con muchos gestos, con superlativos.",
            "valor": 0,
            "total": 0
        },
        {
            "id": 8,
            "pregunta": "REPUESTO PRONTO después de un trabajo agotador. (Recobra pronto sus fuerzas gastadas).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 9,
            "pregunta": "MUY AFECTIVO. (Se estremece fácil y hondamente con el relato de un acto heroico, con la lectura de algo emocionante o al ver una película)",
            "valor": 0,
            "total": 0
        },
        {
            "id": 10,
            "pregunta": "REALIZA SIN DEMORA lo que ha decidido. (Arreglar un asunto, hacer una visita, escribir una carta, etc.).",
            "valor": 0,
            "total": 0
        }
    ],
    "seccion_b": [
        {
            "id": 1,
            "pregunta": "PREVISOR. (Sabe preparar con detalle un programa para el empleo del tiempo, algún plan de fiesta, plan de vida…, y se disgusta ante acontecimeintos imprevistos)",
            "valor": 0,
            "total": 0
        },
        {
            "id": 2,
            "pregunta": "FLEXIBLE. Es fácil adaptación a nuevas condiciones de existencia.",
            "valor": 0,
            "total": 0
        },
        {
            "id": 3,
            "pregunta": "METODICO en sus asustos; siente la necesidad de ordenarlos, de poner cada cosa en su sitio.",
            "valor": 0,
            "total": 0
        },
        {
            "id": 4,
            "pregunta": "Atraído por lo que es FLUIDO, MATIZADO. (Evita las fórmulas tajantes, muy categóricas).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 5,
            "pregunta": "Propenso a haver REVIVIR los acontecimientos pasados (piensa larga y frecuentemente en las misma ideas),",
            "valor": 0,
            "total": 0
        },
        {
            "id": 6,
            "pregunta": "AMPLITUD DE OPINIONES y jucios; considera los diversos aspectos de un hecho, de un asusto.",
            "valor": 0,
            "total": 0
        },
        {
            "id": 7,
            "pregunta": "CONSERVADOR. (Prefiere guardar las normas existentes, la tradición, o tiene la manía de conservar objetos usados, como vestidos, muebles viejeos, cartas antiguas u objetos inúteles).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 8,
            "pregunta": "Habitualmente ACOGEDOR. Tiene tendencia a trabajar espontáneamente buenas relaciones con su interlocutor (actitud de franca simpatía).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 9,
            "pregunta": "Ordinariamente RECATADO, RESERVADO. No le gusta dar a conocer sus amistades, sus simpatia, sus gustos).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 10,
            "pregunta": "Suele PLEGARSE a los acontecimientos, a las circunstancias, a las personas; sabe esperar y encontrar el momento favorable; no fuerza ni precipita.",
            "valor": 0,
            "total": 0
        }
    ],
    "seccion_c": [
        {
            "id": 1,
            "pregunta": "AMBICIOSO: Inclinado a buscar exageradamente y para sí, riqueza, éxito, gloria.",
            "valor": 0,
            "total": 0
        },
        {
            "id": 2,
            "pregunta": "COMPASIVO Y CARITATIVO (se compadece fácilmente de la desgracia ajena).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 3,
            "pregunta": "AVIDO (aspira ardientemente a acrecentar su saber, sus riquezas, su gloria, para poseerlos y gozar de ellos).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 4,
            "pregunta": "Espontáneamente ABNEGADO (pone su alegría en servir a los demás; por ejemplo, en vista a los pobres, o en obras de beneficencia).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 5,
            "pregunta": "RECELOSO (temor de que se prefiera a otro a él, miedo de ser engañado).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 6,
            "pregunta": "BENEVOLO con los inferiores, los empleados. (Se interesa por sus problemas; sabe conquistar su cariño).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 7,
            "pregunta": "CELOSO (manifiesta un vivo apego hacia sus amistades y sus obras).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 8,
            "pregunta": "AFECTUOSO para con los demñas (por ejemplo, le gusta emplear palabras cariñosas; concede mucha importancia a los menores gestos que expresan amistad o amor).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 9,
            "pregunta": "Propenso a APEGARSE exageradamente a sus objetos personales ( experimenta espontáneamente retraimiento y disgusto en prestalos, aunque sea por poco tiempo).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 10,
            "pregunta": "ATENTO (prevee los deseos de los otros).",
            "valor": 0,
            "total": 0
        }
    ],
    "seccion_d": [
        {
            "id": 1,
            "pregunta": "AGRESIVO (le gusta y busca la polémica, la discusión, la competencia).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 2,
            "pregunta": "SOCIABLE (busca la compañía de otros que pertenezcan a su grupo social); toma parte con gusto en reuniones, asambleas.",
            "valor": 0,
            "total": 0
        },
        {
            "id": 3,
            "pregunta": "Aficionado a TOMAR EL MANDO (la dirección de un grupo, de una reunión, de una empresa=.",
            "valor": 0,
            "total": 0
        },
        {
            "id": 4,
            "pregunta": "Interesado por el BIEN COMUN; propenso espontáneamente a colaborar en el écito de una empresa colectiva.",
            "valor": 0,
            "total": 0
        },
        {
            "id": 5,
            "pregunta": "INDEPENDIENTE. Quiere actuar sin consejo, siguiendo su propio criterio.",
            "valor": 0,
            "total": 0
        },
        {
            "id": 6,
            "pregunta": "Atraído por las REUNIONES COLECTIVAS (le gusta las salidas en grupo, las comidas en coún).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 7,
            "pregunta": "ATRAIDO POR EL PELIGRO (inclinación a lo desconocido, con todo los riesdos que supone, complaciendose en ello).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 8,
            "pregunta": "Llevando a COMUNICAR FACILMENTE con los recién llegado (por ejemplo, en una reunión se familiariza rápidamente con los forasteros, le gusta su compañía).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 9,
            "pregunta": "AUTORITATIO. Se impone naturalmente a otro, a un grupo; se hace escuchar, respetar.",
            "valor": 0,
            "total": 0
        },
        {
            "id": 10,
            "pregunta": "SOLIDARIO con su grupo; experimenta respecto de él sentimiento de dependencia, de ayuda mutua.",
            "valor": 0,
            "total": 0
        }
    ],
    "seccion_e": [
        {
            "id": 1,
            "pregunta": "Prefiere las CIENCIAS ABSTRACTAS (Matemáticas, Geometría, Lógica).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 2,
            "pregunta": "Interesado por los PROBLEMAS NUEVOS, originales, que proporcionan a la inteligencia ocasión de ejercitarse, de INQUIRIR O BUSCAR personalmente.",
            "valor": 0,
            "total": 0
        },
        {
            "id": 3,
            "pregunta": "Aficionado a JUEGOS INTELECTUALES (juegos de reflexión, discusion de ideas).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 4,
            "pregunta": "Atraído por todo lo que puede INSTRUIR, extender los conocimientos intelectuales (lecturas, conferencias, documentales).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 5,
            "pregunta": "Da mucha importancia al ASPECTO TEORICO de los problemas (por ejemplo, se interesa más por el principio del funcionamiento de una máquina que por su uso).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 6,
            "pregunta": "Inclinado por naturaleza hacia las COSAS DEL ESPIRITU (frecuenta de buena gana los cículos de estudio, las bibliotecas).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 7,
            "pregunta": "ABUNDANDTE EN IDEAS: las tiene numerosas y variadas sobre temas diferentes.",
            "valor": 0,
            "total": 0
        },
        {
            "id": 8,
            "pregunta": "Capaz de realizar un ESFUERZO PERSONAL y de prolongar para ampliar su cultura. (Se apasiona por los estudios).",
            "valor": 0,
            "total": 0
        },
        {
            "id": 9,
            "pregunta": "En la expresión del pensamiento (redaccion de un trabajo, de una crta, de un informe) se preocupa más por el ORDEN LOGICO de las ideas que por la variedad de las imágenes.",
            "valor": 0,
            "total": 0
        },
        {
            "id": 10,
            "pregunta": "Interesado por la INVESTIGACION de pruebas objetivas (quiere conocer las cosas como son).",
            "valor": 0,
            "total": 0
        }
    ]
}',
            'estado' => 'ACTIVO',
            'id_usuario_creacion' => 1,
            'creacion' => date("Y-m-d H:i:s"),
        ]);
    }
}
