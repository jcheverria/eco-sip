<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterEntrevistaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('entrevistas', function ($table) {
            $table->string('observacion')->after('fecha_inicial_agendada')->nullable();
            $table->string('carrera_seleccionada')->after('observacion')->nullable();
            $table->string('universidad')->after('carrera_seleccionada')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
