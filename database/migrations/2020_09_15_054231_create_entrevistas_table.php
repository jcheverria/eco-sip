<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntrevistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entrevistas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_alumno');
            $table->unsignedBigInteger('id_periodo');
            $table->dateTime('fecha_inicial_agendada')->nullable();
            $table->dateTime('fecha_final_agendada')->nullable();
            $table->enum('estado', ['SIN PRUEBAS', 'NO AGENDADO', 'AGENDADO','ENTREVISTADO', 'NO ENTREVISTADO'])->default('NO AGENDADO');
            //campos estandar
            $table->unsignedBigInteger('id_usuario_creacion');
            $table->unsignedBigInteger('id_usuario_actualizacion')->nullable();
            $table->dateTime('creacion');
            $table->dateTime('actualizacion')->nullable();

            //Relaciones
            $table->foreign('id_usuario_creacion')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_usuario_actualizacion')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_periodo')->references('id')->on('periodos')->onDelete('cascade');
            $table->foreign('id_alumno')->references('id')->on('alumnos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entrevistas');
    }
}
