<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlumnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumnos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_user');
            $table->text('primer_nombre',50);
            $table->text('segundo_nombre',50);
            $table->text('primer_apellido',50);
            $table->text('segundo_apellido',50);
            $table->text('des_curso',50);
            $table->text('paralelo',1);
            $table->dateTime('fecha_nacimiento');
            $table->string('email')->unique();
            $table->string('cod_alumno')->unique();
            $table->unsignedBigInteger('id_periodo');
            $table->enum('estado', ['ACTIVO', 'INACTIVO'])->default('ACTIVO');
            //campos estandar
            $table->unsignedBigInteger('id_usuario_creacion');
            $table->unsignedBigInteger('id_usuario_actualizacion')->nullable();
            $table->dateTime('creacion');
            $table->dateTime('actualizacion')->nullable();

            //Relaciones
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_periodo')->references('id')->on('periodos')->onDelete('cascade');
            $table->foreign('id_usuario_creacion')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_usuario_actualizacion')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnos');
    }
}
