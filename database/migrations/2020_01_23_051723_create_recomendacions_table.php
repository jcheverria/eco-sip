<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecomendacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recomendaciones', function (Blueprint $table) {
            $table->unsignedBigInteger('id_resultado');
            $table->text('recomendacion_1');
            $table->text('recomendacion_2');
            $table->text('recomendacion_3');
            $table->text('selecion');
            $table->text('universidad');
            $table->text('observacion');
            $table->text('psicologa');
            $table->enum('estado', ['ACTIVO', 'INACTIVO'])->default('ACTIVO');
            //campos estandar
            $table->unsignedBigInteger('id_usuario_creacion');
            $table->unsignedBigInteger('id_usuario_actualizacion')->nullable();
            $table->dateTime('creacion');
            $table->dateTime('actualizacion')->nullable();
            //Relaciones
            $table->foreign('id_resultado')->references('id')->on('resultados');
            $table->foreign('id_usuario_creacion')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_usuario_actualizacion')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recomendaciones');
    }
}
