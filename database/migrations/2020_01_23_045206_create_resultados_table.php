<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resultados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_periodo');
            $table->unsignedBigInteger('id_prueba');
            $table->unsignedBigInteger('id_alumno');
            $table->text('resultado_json', 300);
            $table->text('resultado_area', 50);
            $table->text('resultado_id_area', 50);
            $table->enum('estado', ['ACTIVO', 'INACTIVO'])->default('ACTIVO');
            //campos estandar
            $table->unsignedBigInteger('id_usuario_creacion');
            $table->unsignedBigInteger('id_usuario_actualizacion')->nullable();
            $table->dateTime('creacion');
            $table->dateTime('actualizacion')->nullable();
            //Relaciones
            $table->foreign('id_periodo')->references('id')->on('periodos');
            $table->foreign('id_prueba')->references('id')->on('pruebas');
            $table->foreign('id_alumno')->references('id')->on('alumnos');
            $table->foreign('id_usuario_creacion')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_usuario_actualizacion')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resultados');
    }
}
