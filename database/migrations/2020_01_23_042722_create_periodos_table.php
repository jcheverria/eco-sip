<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeriodosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periodos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('periodo_letra', ['MATUTINO', 'VESPERTINO'])->default('MATUTINO');
            $table->enum('estado', ['ACTIVO', 'INACTIVO'])->default('ACTIVO');
            //campos estandar
            $table->unsignedBigInteger('id_usuario_creacion');
            $table->unsignedBigInteger('id_usuario_actualizacion')->nullable();
            $table->dateTime('creacion');
            $table->dateTime('actualizacion')->nullable();

            //Relaciones
            $table->foreign('id_usuario_creacion')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_usuario_actualizacion')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periodos');
    }
}
