/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const entrevistaRoutes = {
  path: '/psicologo',
  component: Layout,
  redirect: '/psicologo/nuevo',
  name: 'psicologo',
  meta: {
    title: 'Entrevista',
    icon: 'education',
    permissions: ['view menu psicologo'],
  },
  children: [
    /** Realizar Matriz */
    {
      path: 'matriz',
      component: () => import('@/views/entrevista/Index'),
      name: 'MatrizList',
      meta: { title: 'matrizEntrevista', icon: 'edit', permissions: ['interview matrix'] },
    },
    /** Resultado entrevista */
    {
      path: 'agendar',
      component: () => import('@/views/entrevista/Agendar'),
      name: 'ScheduleInterview',
      meta: { title: 'Agendar entrevista', icon: 'search', permissions: ['schedule interview'] },
    },
  ],
};

export default entrevistaRoutes;
