/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const generalRoutes = {
  path: '/general',
  component: Layout,
  redirect: '/general/nuevo',
  name: 'general',
  meta: {
    title: 'general',
    icon: 'education',
    permissions: ['view menu general'],
  },
  children: [
    /** Realizar Matriz */
    {
      path: 'periodo',
      component: () => import('@/views/periodos/index'),
      name: 'PeriodoList',
      meta: { title: 'periodo', icon: 'edit', permissions: ['manage psicologa'] },
    },
  ],
};

export default generalRoutes;
