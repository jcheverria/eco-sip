/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const testRoutes = {
  path: '/test',
  component: Layout,
  redirect: '/test/nuevo',
  name: 'Pruebas',
  meta: {
    title: 'test',
    icon: 'education',
    permissions: ['view menu test'],
  },
  children: [
    /* Cipsa */
    {
      path: 'index',
      component: () => import('@/views/test/Test1'),
      name: 'TestList',
      meta: { title: 'indexTest', icon: 'excel', permissions: ['do test cipsa'] },
    },
    /* Pearson */
    {
      path: 'pearson',
      component: () => import('@/views/test/TestPearson'),
      name: 'TestPearson',
      meta: { title: 'Pearson', icon: 'excel', permissions: ['do test pearson'] },
    },

  ],
};

export default testRoutes;
