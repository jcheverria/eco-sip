/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const entrevistaRoutes = {
  path: '/entrevista',
  component: Layout,
  redirect: '/entrevista/nuevo',
  name: 'entrevista',
  meta: {
    title: 'entrevista',
    icon: 'education',
    permissions: ['view menu entrevista'],
  },
  children: [
    /** Realizar Matriz */
    {
      path: 'matriz',
      component: () => import('@/views/table/ComplexTable'),
      name: 'MatrizList',
      meta: { title: 'matrizEntrevista', icon: 'edit', permissions: ['manage psicologa'] },
    },
    /** Resultado entrevista */
    {
      path: 'agendar',
      component: () => import('@/views/tab'),
      name: 'ResultadoList',
      meta: { title: 'resultadoEntrevista', icon: 'search', permissions: ['manage psicologa'] },
    },
  ],
};

export default entrevistaRoutes;
