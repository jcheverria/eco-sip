
import request from '@/utils/request';

export function getIndicadores(query) {
  return request({
    url: '/dasboard/indicadores',
    method: 'get',
    params: query,
  });
}
