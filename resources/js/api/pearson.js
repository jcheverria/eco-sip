import request from '@/utils/request';

export function getPreguntasPearson(query) {
  return request({
    url: '/pearson',
    method: 'get',
    params: query,
  });
}
export function saveRespuestasPearson(data, id) {
  return request({
    url: '/pearson/estudiante',
    method: 'post',
    data: data,
  });
}
