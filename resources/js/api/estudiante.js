import request from '@/utils/request';

export function getNotificaciones(query) {
  return request({
    url: '/estudiantes/notificaciones',
    method: 'get',
    params: query,
  });
}
export function getListaEstudiantes(query) {
  return request({
    url: '/estudiantes/entrevista/matriz',
    method: 'get',
    params: query,
  });
}
export function getCarrera(query) {
  return request({
    url: '/carerras/cipsa',
    method: 'get',
    params: query,
  });
}
export function getAllCarreras(query) {
  return request({
    url: '/carerras',
    method: 'get',
    params: query,
  });
}

