import request from '@/utils/request';

export function getEntrevistas(query) {
  return request({
    url: '/alumnos/entrevistas',
    method: 'get',
    params: query,
  });
}
export function getEntrevistaCalendar(query) {
  return request({
    url: '/alumnos/entrevistas/calendario',
    method: 'get',
    params: query,
  });
}
export function getFiltroEntrevista(query) {
  return request({
    url: '/alumnos/entrevistas/filtro',
    method: 'get',
    params: query,
  });
}
export function saveEvent(query) {
  return request({
    url: '/alumnos/entrevistas/agenda',
    method: 'post',
    params: query,
  });
}
export function saveEntrevista(query) {
  return request({
    url: '/alumnos/entrevistas/resultado',
    method: 'post',
    params: query,
  });
}
export function saveConfirmarEntrevista(query) {
  return request({
    url: '/alumnos/entrevistas/confirmar',
    method: 'post',
    params: query,
  });
}
