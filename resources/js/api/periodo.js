import request from '@/utils/request';

export function fetchListPeriodo(query) {
  return request({
    url: '/periodo',
    method: 'get',
    params: query,
  });
}
export function alumnosPeriodo(query) {
  return request({
    url: '/periodo/periodo/alumnos',
    method: 'get',
    params: query,
  });
}
export function generarEntrevista(query) {
  return request({
    url: '/periodo/generar-entrevistas',
    method: 'get',
    params: query,
  });
}

export function fetchArticle(id) {
  return request({
    url: '/articles/' + id,
    method: 'get',
  });
}

export function fetchPv(id) {
  return request({
    url: '/articles/' + id + '/pageviews',
    method: 'get',
  });
}

export function createPeriodo(data) {
  return request({
    url: '/periodo/create',
    method: 'post',
    data,
  });
}

export function updatePeriodo(data) {
  return request({
    url: '/periodo/update',
    method: 'post',
    data,
  });
}
export function generarTest(data) {
  return request({
    url: '/periodo/genera-test',
    method: 'post',
    data,
  });
}
