import request from '@/utils/request';

export function getPreguntas(query) {
  return request({
    url: '/cipsa',
    method: 'get',
    params: query,
  });
}
export function saveRespuestas(data, id) {
  return request({
    url: '/cipsa/estudiante/' + id,
    method: 'post',
    data: data,
  });
}
