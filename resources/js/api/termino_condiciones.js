import request from '@/utils/request';

export function getTerminos(query) {
  return request({
    url: '/terminos-condiciones',
    method: 'get',
    params: query,
  });
}
