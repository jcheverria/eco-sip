const state = {
  eventofinal: 0,
};

const mutations = {
  SET_EVENTO: (state, valor) => {
    state.eventofinal = valor;
  },
};
const actions = {
  obtenerrespuestas({ commit }, valor) {
    commit('SET_EVENTO', valor);
  },
};
export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
