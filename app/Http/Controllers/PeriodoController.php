<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePeriodo;
use App\Jobs\SendEmailJob;
use App\Laravue\Acl;
use App\Laravue\Faker;
use App\Laravue\JsonResponse;
use App\Laravue\models\Alumno;
use App\Laravue\Models\Entrevista;
use App\Laravue\models\Periodo;
use App\Laravue\models\Resultado;
use App\Laravue\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Laravue\Models\Role;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

use function GuzzleHttp\json_decode;

class PeriodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function fetchListPeriodo()
    {
        //
        $rowsNumber = 10;
        $data = [];
        $periodo = Periodo::all();
        $count = Periodo::where('estado', 'ACTIVO')->count();
        //dd($periodo);
        return response()->json(new JsonResponse(['items' => $periodo, 'total' => $count]));
    }
    public function periodoAlumnos(Request $request)
    {
        set_time_limit(0);
        ini_set("max_execution_time", 600);
        try {
            $periodo_actual = Periodo::where('periodo_letra', $request->periodo_letra)->limit(1)->get();
            $url = env('APIECOMUNDO') . 'alumnos-periodo';
            $curl = curl_init();
            curl_setopt_array(
                $curl,
                array(
                    CURLOPT_URL            => $url,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_TIMEOUT        => 30,
                    CURLOPT_POSTFIELDS     => json_encode(array("periodo" => $request->periodo_letra)),
                    CURLOPT_CUSTOMREQUEST  => "POST",
                    CURLOPT_HTTPHEADER     => array('Content-Type:application/json'),
                )
            );
            $response = curl_exec($curl);
            $error = curl_error($curl);
            $response_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $obj_json_response = json_decode($response);
            if ($error == TRUE) {
                $noticia = ['error' => 'El periodo no existe'];
                return response()->json(['error' => 'El periodo no es correcto'], 500);
            }
            if ($response_status != 200 && $response_status != 201) {
                $noticia = ['error' => 'El periodo no es correcto'];
                return response()->json($noticia, 500);
            }
            //dd(json_encode($response));
        } catch (\Throwable $th) {
            $noticia = ['error' => 'Error'];
            return response()->json($noticia, 500);
        }
        $lista = [];
        foreach (json_decode($response) as $valor) {
            $email = 'e-' . $valor->user_atrium . '@ecomundo.edu.ec';
            array_push($lista, [
                'primer_nombre' => $valor->nombre1,
                'segundo_nombre' => $valor->nombre2,
                'primer_apellido' => $valor->apellido1,
                'segundo_apellido' => $valor->apellido2,
                'nombre_completo' => $valor->nombre1 . ' ' . $valor->nombre2 . ' ' . $valor->apellido1 . ' ' . $valor->apellido2,
                'fecha_nacimiento' => $valor->fecha_nacimiento,
                'email' => $email,
                'cod_alumno' => $valor->cod_alum,
                'id_usuario_creacion' => 1,
                'creacion' => date("Y-m-d H:i:s"),
            ]);
            $periodo_alumnos_user = User::firstOrCreate(
                ['name' => $valor->user_atrium,],
                [
                    'email' => $email,
                    'id_usuario_creacion' => Auth::user()->id,
                    'password' => Hash::make(
                        $valor->cedula_alumno
                    ),
                    'creacion' => date("Y-m-d H:i:s"),
                ]
            );
            $periodo_alumnos = Alumno::firstOrCreate(
                ['cod_alumno' => $valor->cod_alum,],
                [
                    'id_user' => $periodo_alumnos_user->id,
                    'primer_nombre' => $valor->nombre1,
                    'segundo_nombre' => $valor->nombre2,
                    'primer_apellido' => $valor->apellido1,
                    'segundo_apellido' => $valor->apellido2,
                    'nombre_completo' => $valor->nombre1 . ' ' . $valor->nombre2 . ' ' . $valor->apellido1 . ' ' . $valor->apellido2,
                    'fecha_nacimiento' => $valor->fecha_nacimiento,
                    'des_curso' => $valor->des_curso,
                    'paralelo' => $valor->paralelo,
                    'email' => $email,
                    'cod_alumno' => $valor->cod_alum,
                    'id_periodo' => $periodo_actual[0]->id,
                    'id_usuario_creacion' => Auth::user()->id,
                    'creacion' => date("Y-m-d H:i:s"),
                ]
            );
            $role = Role::findByName(Acl::ROLE_ESTUDIANTE);
            $periodo_alumnos_user->syncRoles($role);
            $user_validate = Alumno::where('cod_alumno', $valor->cod_alum)->get();
            if (count($user_validate) < 1) {
                $role = Role::findByName(Acl::ROLE_ESTUDIANTE);
                $periodo_alumnos_user->syncRoles($role);
                if (env('ACTIVAR_CORREOS')) {
                    /* $emails = $email; */
                } else {
                    $emails = ["jorgecheverria96@gmail.com"];
                }
                $nombre = $valor->nombre1;
                /* $nombre = $valor->nombre1;
                // Armar correo y pasarle datos para el constructor
                $correo = new \App\Mail\CorreoBienvenida(['nombre' => $nombre]);
                # ¡Enviarlo!
                Mail ::to($emails)->send($correo); */
                $data['emails'] = $emails;
                $data['nombre'] = $nombre;
                SendEmailJob::dispatch($data)
                    ->delay(Carbon::now()->addSeconds(5));
            }
        }

        return response()->json(json_decode(json_encode($lista)));
    }
    public function updatePeriodo(Request $request)
    {
        //

        try {
            $periodo = Periodo::whereId($request->id);
            $periodo->estado = $request->estado;
            $periodo->save();
        } catch (\Throwable $th) {
            //throw $th;
        }
        return response()->json(['mensaje' => 'ok']);
    }
    public function createPeriodo(Request $request)
    {
        //
        $validacion = Periodo::where('periodo_letra', $request->periodo_letra)
            ->where('jornada', $request->jornada)->get();
        if (count($validacion) > 0) {
            $error = ['error' => 'El periodo no es correcto.'];
            return response()->json(['error' => 'El periodo ya existe'], 500);
        }
        $url = env('APIECOMUNDO') . 'periodo';
        $curl = curl_init();
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL            => $url,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_TIMEOUT        => 30,
                CURLOPT_CUSTOMREQUEST  => "GET",
                CURLOPT_HTTPHEADER     => array('Accept: application/json'),
            )
        );
        $response = curl_exec($curl);
        $error = curl_error($curl);
        $response_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $obj_json_response = json_decode($response);
        if ($error == TRUE) {
            $noticia = [
                'titulo' => 'Error',
                'mensaje' => 'Error',
            ];
            return response()->json($noticia, 500);
        }
        if ($response_status != 200 && $response_status != 201) {
            $noticia = [
                'titulo' => 'Error',
                'mensaje' => 'Error',
            ];
            return response()->json($noticia, 500);
        }
        if ($response_status == 200) {
            $periodo = $obj_json_response[0]->periodo;
        }
        if ($request->periodo_letra == $periodo) {
            $nuevo_periodo = Periodo::create([
                'periodo_letra' => $request->periodo_letra,
                'jornada' => $request->jornada,
                'estado' => 'ACTIVO',
                'id_usuario_creacion' => Auth::user()->id,
            ]);
        } else {
            return response()->json(['error' => 'El periodo no es correcto'], 500);
        }
        return response()->json(['mensaje' => 'ok', 'id' => $nuevo_periodo->id], 200);
    }

    public function generarEntrevista(Request $request)
    {

        $estado = $request->estado;
        $periodo_letra = $request->periodo_letra;

        $id_periodo = Periodo::where('periodo_letra', $periodo_letra)->get()[0]->id;

        $alumnos_periodo = Alumno::where('id_periodo', $id_periodo)->get();
        foreach ($alumnos_periodo as $key => $value) {
            $resultado_pruebas = Resultado::where('id_alumno', $value->id)->get()->toArray();
            if (count($resultado_pruebas) > 0) {
                $entrevista = Entrevista::updateOrCreate(
                    ['id_alumno' => $value->id, 'id_periodo' => $value->id_periodo, 'estado' => 'SIN PRUEBAS'],
                    [
                        'id_alumno' => $value->id,
                        'id_periodo' => $value->id_periodo,
                        'estado' => 'NO AGENDADO',
                        'id_usuario_creacion' => Auth::user()->id,
                    ]
                );
                /* $entrevista = Entrevista::where('id_alumno', $value->id)->where('id_periodo', $value->id_periodo)->where('estado', 'SIN PRUEBAS')->update([
                    'estado' => 'NO AGENDADO'
                ]); */
            } else {
                $entrevista = Entrevista::firstOrCreate(
                    ['id_alumno' => $value->id, 'id_periodo' => $value->id_periodo],
                    [
                        'id_alumno' => $value->id,
                        'id_periodo' => $value->id_periodo,
                        'estado' => 'SIN PRUEBAS',
                        'id_usuario_creacion' => Auth::user()->id,
                    ]
                );
            }
        }
        $mensaje = [
            'mensaje' => 'Se guardó con éxito las entrevistas'
        ];
        return response()->json($mensaje);
    }

    public function generarTest(Request $request)
    {
        if ($request[1] == 'cipsa') {
            $alumnos = Alumno::where('id_periodo', $request[0]['id'])
                ->with('user')
                ->with('resultado')
                ->get();
            foreach ($alumnos as $key => $value) {
                if (count($value->resultado) > 0) {
                } else {
                    $value->user->givePermissionTo('do test cipsa');
                }
            }
        } else {
            $alumnos = Alumno::where('id_periodo', $request[0]['id'])
                ->with('user')
                ->with('resultado')
                ->get();
            foreach ($alumnos as $key => $value) {
                if (count($value->resultado) > 0) {
                } else {
                    $value->user->givePermissionTo('do test pearson');
                }
            }
        }
        echo 'ok';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\models\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function show(Periodo $periodo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\models\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function edit(Periodo $periodo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\models\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Periodo $periodo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\models\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Periodo $periodo)
    {
        //
    }
}
