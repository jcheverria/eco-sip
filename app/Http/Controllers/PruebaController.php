<?php

namespace App\Http\Controllers;

use App\Laravue\JsonResponse;
use App\Laravue\models\Alumno;
use App\Laravue\models\Prueba;
use App\Laravue\models\Resultado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PruebaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPreguntas()
    {
        $pruebas = Prueba::where('descripcion', 'CIPSA')->get();
        //dd($pruebas[0]['formato']);
        return response()->json(json_decode($pruebas[0]['formato']));
    }
    public function saveRespuestas($id, Request $request)
    {
        set_time_limit(60);
        try {
            $json = [];
            array_push($json, ($request->seccion1));
            array_push($json, ($request->seccion2));
            array_push($json, ($request->seccion3));
            $inicio = 1;
            $numArregloDeta  = 0;
            for ($k = 0; $k < 3; $k++) { //secciones
                for ($j = $inicio; $j < $inicio + 4; $j++) { // valida 4 areas por grupo
                    $totalArea = 0;
                    $area = 'area' . strval($j);
                    for ($i = 0; $i < 16; $i++) { //carreras
                        $subTotal = 0;
                        $personal = $json[$k][$area][$i]["personal"];
                        $social = $json[$k][$area][$i]["social"];
                        $economico = $json[$k][$area][$i]["economico"];

                        switch (strval($personal)) {
                            case "D":
                                $subTotal = $subTotal +  1;
                                break;
                            case "I":
                                $subTotal = $subTotal +  2;
                                break;
                            case "R":
                                $subTotal = $subTotal +  3;
                                break;
                            case "E":
                                $subTotal = $subTotal +  4;
                                break;
                            case "0":
                                $subTotal = $subTotal +  3;
                                break;
                            default:
                                $subTotal = $subTotal +  3;
                                break;
                        }
                        $subTotal = $subTotal + intval($social) + intval($economico); //total de la carrera
                        $totalDetallada[$numArregloDeta + $i] = $subTotal; // total por carrera  en un arreglo                
                        $totalArea = $totalArea  + $subTotal; //acumulado del area                         
                    }
                    $totalAreas[$j] = $totalArea;    // un arreglo el total de areas                        
                    $numArregloDeta = 16 * $j;
                }
                $inicio = $inicio + 4;
            }
            $arreglo_resultado = '"';
            //dd(count($totalAreas));
            foreach ($totalAreas as $key => $value) {
                if (count($totalAreas) == $key) {
                    $arreglo_resultado = $arreglo_resultado . $value . '"';
                } else {
                    $arreglo_resultado = $arreglo_resultado . $value . ',';
                }
            }
            //echo $arreglo_resultado;
            $salida = exec('python D:/datos/laravel/eco-sip/storage/modelos/cipsa.py ' . $arreglo_resultado);
            $salida = ltrim($salida, '[');
            $salida = rtrim($salida, ']');
            $areas = [
                'ACTIVIDADES LITERARIAS', 'ARTES PLASTIAS Y MUSICA',
                'CIENCIAS BIOLOGICAS', 'CIENCIAS HUMANAS', 'DERECHO Y LEGISLACION',
                'ENSEÑANZA', 'FISICO - QUIMICA', 'MEDICINA O SANIDAD',
                'ORANIZACION Y MANDO', 'PUBLICIDAD Y COMUNICACIÓN',
                'RELACIONES ECONOMICAS Y EMPRESARIALES', 'SERVICIOS'
            ];
            //echo '---------------'.$salida.'------';
            /* dd($areas[$salida]);
            dd('ok');  
            return;    */
            //var_dump($totalAreas); // presneta los resultados por areas 
            //var_dump($totalDetallada); 
            $recomendacion = array_search(max($totalAreas), $totalAreas);
            $categorizado = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            $categorizado[$recomendacion - 1] = 1;
            $analizar = array_merge($totalDetallada, $categorizado);
            // var_dump($analizar);

            //return response()->json(json_decode(json_encode($json)));
            $id_alumno = Alumno::where(
                'id_user',
                Auth::user()->id
            )->get()[0];
            $resultado = Resultado::create([
                'id_periodo' => 1,
                'id_prueba' => 2,
                'id_alumno' => $id_alumno->id,
                'resultado_json' => json_encode($json),
                'resultado_area' => $areas[$salida],
                'resultado_id_area' => $salida,
                'id_usuario_creacion' => Auth::user()->id,
            ]);
            Auth::user()->revokePermissionTo('do test cipsa');
            $mensaje = [
                'contenido' => 'Se registro con éxito',
            ];
            return response()->json($mensaje);
        } catch (\Throwable $th) {
            $mensaje = [
                'contenido' => 'Hubo un error al guardar la prueba',
                'error' => $th,
            ];
            return response()->json($mensaje);
        }
    }

    public function getPreguntasPearson()
    {
        $pearson = Prueba::where('descripcion', 'PEARSON')->get();
        //dd($pearson[0]['formato']);
        return response()->json((["0" => json_decode($pearson[0]['formato'])]));
    }
    public function saveRespuestasPearson(Request $request)
    {
        try {
            $contenido = $request->secciones;
            $index = 0;
            for ($i = 65; $i <= 69; $i++) { // letras de la a-b-c-d-e
                $seccion = 'seccion_' . strtolower(chr($i));
                $pares = 0;
                $impares = 0;
                for ($j = 0; $j < 10; $j++) {
                    if (fmod($j, 2) == 0) {
                        $pares = $pares + intval($contenido[$seccion][$j]["valor"]);
                    } else {
                        $impares = $impares + intval($contenido[$seccion][$j]["valor"]);
                    }
                }
                $arrayTotal[$index] = $pares;
                $arrayTotal[$index + 1] = $impares;
                $index = $index + 2;
            }

            if ($arrayTotal[0] <= 10) {
                $arregloPearson[0] = 'NE';
            } else {
                $arregloPearson[0] = 'E';
            }

            if ($arrayTotal[1] <= 10) {
                $arregloPearson[1] = 'NA';
            } else {
                $arregloPearson[1] = 'A';
            }

            if ($arrayTotal[2] <= 10) {
                $arregloPearson[2] = 'P';
            } else {
                $arregloPearson[2] = 'S';
            }
            $tiposPearson = array('EAP', 'EAS', 'NEAP', 'NEAS', 'ENAP', 'ENAS', 'NENAP', 'NENAS');
            $personalidad = array('COLERICO', 'APASIONADO', 'NERVIOSO', 'SENTIMENTAL', 'SANGUINEO', 'FLEMATICO', 'AMORFO', 'APATICO');
            $resPearson = implode('', $arregloPearson);
            $num = array_search($resPearson, $tiposPearson);
            //var_dump($arrayTotal);
            $respuesta_pearson = '"' . $arrayTotal[0] . ',' . $arrayTotal[1] . ',' . $arrayTotal[2] . '"';

            $salida = exec('python D:/datos/laravel/eco-sip/storage/modelos/pearson.py ' . $respuesta_pearson);
            $salida = ltrim($salida, '[');
            $salida = rtrim($salida, ']');
            $areas = [
                'AMORFO', 'APASIONADO', 'APATICO', 'COLERICO', 'FLEMATICO',
                'NERVIOSO', 'SANGUINEO', 'SENTIMENTAL'
            ];
            $id_alumno = Alumno::where('id_user', Auth::user()->id)->get()[0];
            $resultado = Resultado::create([
                'id_periodo' => 1,
                'id_prueba' => 1,
                'id_alumno' => $id_alumno->id,
                'resultado_json' => json_encode($contenido),
                'resultado_area' => $areas[$salida],
                'resultado_id_area' => $salida,
                'id_usuario_creacion' => Auth::user()->id,
            ]);
            $mensaje = [
                'contenido' => 'Se registro con éxito',
            ];
            Auth::user()->revokePermissionTo('do test pearson');
            return response()->json($mensaje);
        } catch (\Throwable $th) {
            $mensaje = [
                'contenido' => 'Hubo un error al guardar la prueba',
                'error' => $th,
            ];
            return response()->json($mensaje);
        }
    }

    public function modelo_cipsa()
    {
        /* if(env('ACTIVAR_CORREOS')){
            dd('correos activador');
        }else{
            dd('correos inactivos');
        }
        $destinatario = "jorgecheverria96@gmail.com";
        $nombre = "Luis Cabrera Benito";
        // Armar correo y pasarle datos para el constructor
        $correo = new \App\Mail\AgendaEntrevista($nombre);
        # ¡Enviarlo!
        Mail::to($destinatario)->send($correo);
        return; */
        $salida = exec('python D:/datos/laravel/eco-sip/storage/modelos/cipsa.py "60,129,33,46,41,59,56,86,58,78,48,87"');
        $salida = ltrim($salida, '[');
        $salida = rtrim($salida, ']');
        $areas = [
            'ACTIVIDADES LITERARIAS', 'ARTES PLASTIAS Y MUSICA',
            'CIENCIAS BIOLOGICAS', 'CIENCIAS HUMANAS', 'DERECHO Y LEGISLACION',
            'ENSEÑANZA', 'FISICO - QUIMICA', 'MEDICINA O SANIDAD',
            'ORANIZACION Y MANDO', 'PUBLICIDAD Y COMUNICACIÓN',
            'RELACIONES ECONOMICAS Y EMPRESARIALES', 'SERVICIOS'
        ];
        dd($areas[$salida]);
    }

    public function getcarrerascipsa(Request $request)
    {
        $carreas = [
            "FISICO - QUIMICA" => [
                ["carrera" => "Químico"],
                ["carrera" => "Ingeniero electrónico"],
                ["carrera" => "Matemático"],
                ["carrera" => "Técnico Informático"],
                ["carrera" => "Ingeniero naval"],
                ["carrera" => "Programador de sistemas"],
                ["carrera" => "Ingeniero en caminos"],
                ["carrera" => "Ingeniero en telecomunicaciones"],
                ["carrera" => "Astrónomo"],
                ["carrera" => "Técnico en electrónica"],
                ["carrera" => "Ingeniero de minas"],
                ["carrera" => "Proyectista de maquinaria"],
                ["carrera" => "Físico nuclear"],
                ["carrera" => "Ingeniero aeronáutica"],
                ["carrera" => "Estadístico"],
                ["carrera" => "Ingeniero espacial"],
            ],
            "DERECHO Y LEGISLACION" => [
                ["carrera" => "Juez"],
                ["carrera" => "Diplomático"],
                ["carrera" => "Asesor ejecutivo"],
                ["carrera" => "Secretario de juzgado"],
                ["carrera" => "Concejal"],
                ["carrera" => "Abogado laboratorista"],
                ["carrera" => "Miembro ejecutiva partido política"],
                ["carrera" => "Diputado"],
                ["carrera" => "Abogado criminalista"],
                ["carrera" => "Senador"],
                ["carrera" => "Embajador"],
                ["carrera" => "Funcionario de ministerios"],
                ["carrera" => "Político"],
                ["carrera" => "Abogado administrativo"],
                ["carrera" => "Ministro"],
                ["carrera" => "Secretario de ayuntamiento"],
            ],
            "MEDICINA O SANIDAD" => [
                ["carrera" => "Médico de medicina general"],
                ["carrera" => "Dermatólogo"],
                ["carrera" => "Enfermero"],
                ["carrera" => "Psiquiatra"],
                ["carrera" => "Neumólogo"],
                ["carrera" => "Oftalmólogo"],
                ["carrera" => "Practicante"],
                ["carrera" => "Ginecólogo"],
                ["carrera" => "Fisiólogo"],
                ["carrera" => "Anestesista"],
                ["carrera" => "Endocrino"],
                ["carrera" => "Radiólogo"],
                ["carrera" => "Analista"],
                ["carrera" => "Otorrino"],
                ["carrera" => "A.T.S (Ayudante Técnico Sanitario)"],
                ["carrera" => "Cirujano"],
            ],
            "SERVICIOS" => [
                ["carrera" => "Inspector de Policía"],
                ["carrera" => "Mecánico"],
                ["carrera" => "Albañil"],
                ["carrera" => "Peluquero"],
                ["carrera" => "Policía municipal"],
                ["carrera" => "Conductor de servicio público"],
                ["carrera" => "Bombero"],
                ["carrera" => "Azafato"],
                ["carrera" => "Cartero"],
                ["carrera" => "Fontanero"],
                ["carrera" => "Empleado del servicio de basuras"],
                ["carrera" => "Electricista"],
                ["carrera" => "Empleado de servicios - públicos"],
                ["carrera" => "Guarda Civil"],
                ["carrera" => "Esteticista"],
                ["carrera" => "Carpintero"],
            ],
            "CIENCIAS HUMANAS" => [
                ["carrera" => "Historiador"],
                ["carrera" => "Etnólogo"],
                ["carrera" => "Auxiliar de puericultura"],
                ["carrera" => "Sociología"],
                ["carrera" => "Misionero"],
                ["carrera" => "Psicólogo"],
                ["carrera" => "Filólogo"],
                ["carrera" => "Arqueólogo"],
                ["carrera" => "Antropólogo"],
                ["carrera" => "Asistente social"],
                ["carrera" => "Lingüista"],
                ["carrera" => "Religioso"],
                ["carrera" => "Asesor familiar"],
                ["carrera" => "Filósofo"],
                ["carrera" => "Orientador Psicopedagogo"],
                ["carrera" => "Sacerdote"],
            ],
            "CIENCIAS BIOLOGICAS" => [
                ["carrera" => "Zoólogo"],
                ["carrera" => "Bioquímico"],
                ["carrera" => "Ecólogo"],
                ["carrera" => "Ganadero"],
                ["carrera" => "Botánico"],
                ["carrera" => "Perito agrícola"],
                ["carrera" => "Veterinario"],
                ["carrera" => "Geólogo"],
                ["carrera" => "Ingeniero agrónomo"],
                ["carrera" => "Farmacéutico"],
                ["carrera" => "Bacteriólogo"],
                ["carrera" => "Floricultor"],
                ["carrera" => "Biólogo"],
                ["carrera" => "Técnico agropecuario"],
                ["carrera" => "Oceanógrafo"],
                ["carrera" => "Agricultor"],
            ],
            "ACTIVIDADES LITERARIAS" => [
                ["carrera" => "Novelista"],
                ["carrera" => "Autor teatral"],
                ["carrera" => "Crítico musical"],
                ["carrera" => "Ensayista"],
                ["carrera" => "Periodista"],
                ["carrera" => "Crítico literario"],
                ["carrera" => "Corresponsal en el extranjero"],
                ["carrera" => "Crítico de deportes"],
                ["carrera" => "Crítico de cine"],
                ["carrera" => "Reportero"],
                ["carrera" => "Escritor narrativo"],
                ["carrera" => "Académico"],
                ["carrera" => "Poeta"],
                ["carrera" => "Guionista"],
                ["carrera" => "Crítico de arte"],
                ["carrera" => "Redactor de periódico"],
            ],
            "PUBLICIDAD Y COMUNICACIÓN" => [
                ["carrera" => "Técnico de publicidad"],
                ["carrera" => "Redactor de publicidad"],
                ["carrera" => "Dibujante"],
                ["carrera" => "Diseñador de moda"],
                ["carrera" => "Relaciones Públicas"],
                ["carrera" => "Presentador de televisión"],
                ["carrera" => "Técnico de sonido"],
                ["carrera" => "Coreógrafo"],
                ["carrera" => "Agende de publicidad"],
                ["carrera" => "Locutor de radio"],
                ["carrera" => "Delineante"],
                ["carrera" => "Animador de espectáculos"],
                ["carrera" => "Escenógrafo"],
                ["carrera" => "Modelo"],
                ["carrera" => "Disc-jockey"],
                ["carrera" => "Investigador de mercados"],
            ],
            "ARTES PLASTIAS Y MUSICA" => [
                ["carrera" => "Cantante de ópera - coro"],
                ["carrera" => "Instrumentista"],
                ["carrera" => "Humorista"],
                ["carrera" => "Bailarín"],
                ["carrera" => "Fotógrafo"],
                ["carrera" => "Ceramista"],
                ["carrera" => "Compositor musical"],
                ["carrera" => "Pintor artístico"],
                ["carrera" => "Caricatura"],
                ["carrera" => "Profesor de orquesta"],
                ["carrera" => "Escultor"],
                ["carrera" => "Actor"],
                ["carrera" => "Decorador artístico"],
                ["carrera" => "Director de orquesta"],
                ["carrera" => "Arquitecto"],
                ["carrera" => "Cantante solista"],
            ],
            "ORANIZACION Y MANDO" => [
                ["carrera" => "Alcalde"],
                ["carrera" => "Director de hotel"],
                ["carrera" => "Programador de radio o televisión"],
                ["carrera" => "Jefe de Policía municipal"],
                ["carrera" => "Piloto de vuelo"],
                ["carrera" => "Jefe de guardia civil"],
                ["carrera" => "Director de colegio"],
                ["carrera" => "Jefe de personal"],
                ["carrera" => "Jefe de Policía Nacional"],
                ["carrera" => "Director teatral"],
                ["carrera" => "Comisario de policía"],
                ["carrera" => "Capitán de barco"],
                ["carrera" => "Gobernador"],
                ["carrera" => "Director de cine"],
                ["carrera" => "Realizador de televisión"],
                ["carrera" => "Controlador aéreo"],
            ],
            "ENSEÑANZA" => [
                ["carrera" => "Profesor de formación profesional"],
                ["carrera" => "Profesor de Educación Física"],
                ["carrera" => "Profesor de ciencias naturales"],
                ["carrera" => "Profesor de Física"],
                ["carrera" => "Profesor de Dibujo"],
                ["carrera" => "Profesor de Música"],
                ["carrera" => "Profesor de Idiomas"],
                ["carrera" => "Profesor de Educación especial"],
                ["carrera" => "Profesor de Primaria/Secundaria"],
                ["carrera" => "Profesor de Historia"],
                ["carrera" => "Profesor  de Lenguaje"],
                ["carrera" => "Profesor de Lenguas clásicas"],
                ["carrera" => "Profesor de Universidad"],
                ["carrera" => "Profesor de Arte"],
                ["carrera" => "Profesor de Matemáticas"],
                ["carrera" => "Profesor de Química"],
            ],
            "RELACIONES ECONOMICAS Y EMPRESARIALES" => [
                ["carrera" => "Cajero"],
                ["carrera" => "Interventor de banco"],
                ["carrera" => "Productor de cine"],
                ["carrera" => "Director de banco"],
                ["carrera" => "Auxiliar administrativo"],
                ["carrera" => "Gerente"],
                ["carrera" => "Director de ventas"],
                ["carrera" => "Empleado de comercio"],
                ["carrera" => "Secretario"],
                ["carrera" => "Contable"],
                ["carrera" => "Director de empresa"],
                ["carrera" => "Economista"],
                ["carrera" => "Técnico de marketing"],
                ["carrera" => "Intendente comercial"],
                ["carrera" => "Agente inmobiliaria"],
                ["carrera" => "Jefe de ventas o compras"],
            ],
            "RELACIONES ECONOMICAS Y EMPRESARIALES" => [
                ["carrera" => "Cajero"],
                ["carrera" => "Interventor de banco"],
                ["carrera" => "Productor de cine"],
                ["carrera" => "Director de banco"],
                ["carrera" => "Auxiliar administrativo"],
                ["carrera" => "Gerente"],
                ["carrera" => "Director de ventas"],
                ["carrera" => "Empleado de comercio"],
                ["carrera" => "Secretario"],
                ["carrera" => "Contable"],
                ["carrera" => "Director de empresa"],
                ["carrera" => "Economista"],
                ["carrera" => "Técnico de marketing"],
                ["carrera" => "Intendente comercial"],
                ["carrera" => "Agente inmobiliaria"],
                ["carrera" => "Jefe de ventas o compras"],
            ],
            "COLERICO" => [
                ["carrera" => "Abogado"],
                ["carrera" => "Diplomativo"],
                ["carrera" => "Orador"],
                ["carrera" => "Periodista"],
                ["carrera" => "Escritor"],
                ["carrera" => "Politico"],
                ["carrera" => "Medico"],
                ["carrera" => "Ciencias Sociales"],
                ["carrera" => "Personalidad: Alegre, buen humor, expansivo, habil, astuto, cofiado, voluble, impulsivo, violento, hablador, extrovertido, inteligencia rapido, cualidadesde de lider"],
                ["carrera" => "Area: Sociales"],
            ],
            "APASIONADO" => [
                ["carrera" => "Fisico - matematico"],
                ["carrera" => "ciencias teoricas"],
                ["carrera" => "Ingeneiro"],
                ["carrera" => "Arquitecto"],
                ["carrera" => "Constructor"],
                ["carrera" => "Aplicado al trabajo, bueno para negocios, compasivo, sencillo, desconfiado, relfexivo,testarudo, impaciente gusta de deportes"],
                ["carrera" => "Area: FIMA"],
            ],
            "NERVIOSO" => [
                ["carrera" => "Carreras artisticas"],
                ["carrera" => "Escritor"],
                ["carrera" => "Periodista"],
                ["carrera" => "Pintor"],
                ["carrera" => "Decorador"],
                ["carrera" => "Cine"],
                ["carrera" => "Teatro"],
                ["carrera" => "Impulsivo, violento, susceptible, coluble, inestable, inconstante, artista, tenso, bondadoso, generoso, servicial, surperficial."],
                ["carrera" => "Area: Sociales - Arte"],
            ],
            "SENTIMENTAL" => [
                ["carrera" => "Profesor"],
                ["carrera" => "Medico"],
                ["carrera" => "Psicologo"],
                ["carrera" => "Escritor"],
                ["carrera" => "Quimico"],
                ["carrera" => "Biologo"],
                ["carrera" => "Meditativo, preocupado, introvertido, ansioso, fragilidad, timido, desconfiado, sensible, susceptible, pasivo, soñador, honrado, bondadoso."],
                ["carrera" => "Area: Quibio"],
            ],
            "SANGUINEO" => [
                ["carrera" => "Medicina General"],
                ["carrera" => "Abogado"],
                ["carrera" => "Banquero"],
                ["carrera" => "Periodista"],
                ["carrera" => "Ingeniero agronomo"],
                ["carrera" => "Literatura"],
                ["carrera" => "Calmado, decidido, sereno, diplomatico, astuto, egosita, amigable, afable, servicial, misericordioso"],
                ["carrera" => "Area: Quibio -Sociales"],
            ],
            "FLEMATICO" => [
                ["carrera" => "Juez"],
                ["carrera" => "Ingeneiro"],
                ["carrera" => "Medico"],
                ["carrera" => "Odontolgo"],
                ["carrera" => "Banquero"],
                ["carrera" => "Administrador contable"],
                ["carrera" => "Contador"],
                ["carrera" => "tolerante, coherente, puntual, minucioso, metodico, sincero, alegre, honrado, despreocupado, insensible, frio"],
                ["carrera" => "Area: FIMA - Quibio - Comercio"],
            ],
            "AMORFO" => [
                ["carrera" => "Farmaceutico"],
                ["carrera" => "Quimico"],
                ["carrera" => "Trabajos de oficina"],
                ["carrera" => "Secretario"],
                ["carrera" => "compasivo, tolerante, optimista, amable, sociable, valiente, pasible, impertubable, impuntual, perezosos, incumplido"],
                ["carrera" => "Area: Secretario - Quibio"],
            ],
            "APATICO" => [
                ["carrera" => "Farmaceutico"],
                ["carrera" => "veterinario"],
                ["carrera" => "odontologo"],
                ["carrera" => "callado, calmado, conservador, interesado, introvertido, carece de entusiasmo"],
                ["carrera" => "Area: Secretario - Quibio"],
            ],
        ];
        return ($carreas[$request[0]]);
    }
    public function getallcarreras(Request $request)
    {
        $resultados = Resultado::where('id_alumno', $request[0])->get();
        $cipsa = [];
        $pearson = [];
        foreach ($resultados as $key => $value) {
            $carreas = [
                "FISICO - QUIMICA" => [
                    ["carrera" => "Químico"],
                    ["carrera" => "Ingeniero electrónico"],
                    ["carrera" => "Matemático"],
                    ["carrera" => "Técnico Informático"],
                    ["carrera" => "Ingeniero naval"],
                    ["carrera" => "Programador de sistemas"],
                    ["carrera" => "Ingeniero en caminos"],
                    ["carrera" => "Ingeniero en telecomunicaciones"],
                    ["carrera" => "Astrónomo"],
                    ["carrera" => "Técnico en electrónica"],
                    ["carrera" => "Ingeniero de minas"],
                    ["carrera" => "Proyectista de maquinaria"],
                    ["carrera" => "Físico nuclear"],
                    ["carrera" => "Ingeniero aeronáutica"],
                    ["carrera" => "Estadístico"],
                    ["carrera" => "Ingeniero espacial"],
                ],
                "DERECHO Y LEGISLACION" => [
                    ["carrera" => "Juez"],
                    ["carrera" => "Diplomático"],
                    ["carrera" => "Asesor ejecutivo"],
                    ["carrera" => "Secretario de juzgado"],
                    ["carrera" => "Concejal"],
                    ["carrera" => "Abogado laboratorista"],
                    ["carrera" => "Miembro ejecutiva partido política"],
                    ["carrera" => "Diputado"],
                    ["carrera" => "Abogado criminalista"],
                    ["carrera" => "Senador"],
                    ["carrera" => "Embajador"],
                    ["carrera" => "Funcionario de ministerios"],
                    ["carrera" => "Político"],
                    ["carrera" => "Abogado administrativo"],
                    ["carrera" => "Ministro"],
                    ["carrera" => "Secretario de ayuntamiento"],
                ],
                "MEDICINA O SANIDAD" => [
                    ["carrera" => "Médico de medicina general"],
                    ["carrera" => "Dermatólogo"],
                    ["carrera" => "Enfermero"],
                    ["carrera" => "Psiquiatra"],
                    ["carrera" => "Neumólogo"],
                    ["carrera" => "Oftalmólogo"],
                    ["carrera" => "Practicante"],
                    ["carrera" => "Ginecólogo"],
                    ["carrera" => "Fisiólogo"],
                    ["carrera" => "Anestesista"],
                    ["carrera" => "Endocrino"],
                    ["carrera" => "Radiólogo"],
                    ["carrera" => "Analista"],
                    ["carrera" => "Otorrino"],
                    ["carrera" => "A.T.S (Ayudante Técnico Sanitario)"],
                    ["carrera" => "Cirujano"],
                ],
                "SERVICIOS" => [
                    ["carrera" => "Inspector de Policía"],
                    ["carrera" => "Mecánico"],
                    ["carrera" => "Albañil"],
                    ["carrera" => "Peluquero"],
                    ["carrera" => "Policía municipal"],
                    ["carrera" => "Conductor de servicio público"],
                    ["carrera" => "Bombero"],
                    ["carrera" => "Azafato"],
                    ["carrera" => "Cartero"],
                    ["carrera" => "Fontanero"],
                    ["carrera" => "Empleado del servicio de basuras"],
                    ["carrera" => "Electricista"],
                    ["carrera" => "Empleado de servicios - públicos"],
                    ["carrera" => "Guarda Civil"],
                    ["carrera" => "Esteticista"],
                    ["carrera" => "Carpintero"],
                ],
                "CIENCIAS HUMANAS" => [
                    ["carrera" => "Historiador"],
                    ["carrera" => "Etnólogo"],
                    ["carrera" => "Auxiliar de puericultura"],
                    ["carrera" => "Sociología"],
                    ["carrera" => "Misionero"],
                    ["carrera" => "Psicólogo"],
                    ["carrera" => "Filólogo"],
                    ["carrera" => "Arqueólogo"],
                    ["carrera" => "Antropólogo"],
                    ["carrera" => "Asistente social"],
                    ["carrera" => "Lingüista"],
                    ["carrera" => "Religioso"],
                    ["carrera" => "Asesor familiar"],
                    ["carrera" => "Filósofo"],
                    ["carrera" => "Orientador Psicopedagogo"],
                    ["carrera" => "Sacerdote"],
                ],
                "CIENCIAS BIOLOGICAS" => [
                    ["carrera" => "Zoólogo"],
                    ["carrera" => "Bioquímico"],
                    ["carrera" => "Ecólogo"],
                    ["carrera" => "Ganadero"],
                    ["carrera" => "Botánico"],
                    ["carrera" => "Perito agrícola"],
                    ["carrera" => "Veterinario"],
                    ["carrera" => "Geólogo"],
                    ["carrera" => "Ingeniero agrónomo"],
                    ["carrera" => "Farmacéutico"],
                    ["carrera" => "Bacteriólogo"],
                    ["carrera" => "Floricultor"],
                    ["carrera" => "Biólogo"],
                    ["carrera" => "Técnico agropecuario"],
                    ["carrera" => "Oceanógrafo"],
                    ["carrera" => "Agricultor"],
                ],
                "ACTIVIDADES LITERARIAS" => [
                    ["carrera" => "Novelista"],
                    ["carrera" => "Autor teatral"],
                    ["carrera" => "Crítico musical"],
                    ["carrera" => "Ensayista"],
                    ["carrera" => "Periodista"],
                    ["carrera" => "Crítico literario"],
                    ["carrera" => "Corresponsal en el extranjero"],
                    ["carrera" => "Crítico de deportes"],
                    ["carrera" => "Crítico de cine"],
                    ["carrera" => "Reportero"],
                    ["carrera" => "Escritor narrativo"],
                    ["carrera" => "Académico"],
                    ["carrera" => "Poeta"],
                    ["carrera" => "Guionista"],
                    ["carrera" => "Crítico de arte"],
                    ["carrera" => "Redactor de periódico"],
                ],
                "PUBLICIDAD Y COMUNICACIÓN" => [
                    ["carrera" => "Técnico de publicidad"],
                    ["carrera" => "Redactor de publicidad"],
                    ["carrera" => "Dibujante"],
                    ["carrera" => "Diseñador de moda"],
                    ["carrera" => "Relaciones Públicas"],
                    ["carrera" => "Presentador de televisión"],
                    ["carrera" => "Técnico de sonido"],
                    ["carrera" => "Coreógrafo"],
                    ["carrera" => "Agende de publicidad"],
                    ["carrera" => "Locutor de radio"],
                    ["carrera" => "Delineante"],
                    ["carrera" => "Animador de espectáculos"],
                    ["carrera" => "Escenógrafo"],
                    ["carrera" => "Modelo"],
                    ["carrera" => "Disc-jockey"],
                    ["carrera" => "Investigador de mercados"],
                ],
                "ARTES PLASTIAS Y MUSICA" => [
                    ["carrera" => "Cantante de ópera - coro"],
                    ["carrera" => "Instrumentista"],
                    ["carrera" => "Humorista"],
                    ["carrera" => "Bailarín"],
                    ["carrera" => "Fotógrafo"],
                    ["carrera" => "Ceramista"],
                    ["carrera" => "Compositor musical"],
                    ["carrera" => "Pintor artístico"],
                    ["carrera" => "Caricatura"],
                    ["carrera" => "Profesor de orquesta"],
                    ["carrera" => "Escultor"],
                    ["carrera" => "Actor"],
                    ["carrera" => "Decorador artístico"],
                    ["carrera" => "Director de orquesta"],
                    ["carrera" => "Arquitecto"],
                    ["carrera" => "Cantante solista"],
                ],
                "ORANIZACION Y MANDO" => [
                    ["carrera" => "Alcalde"],
                    ["carrera" => "Director de hotel"],
                    ["carrera" => "Programador de radio o televisión"],
                    ["carrera" => "Jefe de Policía municipal"],
                    ["carrera" => "Piloto de vuelo"],
                    ["carrera" => "Jefe de guardia civil"],
                    ["carrera" => "Director de colegio"],
                    ["carrera" => "Jefe de personal"],
                    ["carrera" => "Jefe de Policía Nacional"],
                    ["carrera" => "Director teatral"],
                    ["carrera" => "Comisario de policía"],
                    ["carrera" => "Capitán de barco"],
                    ["carrera" => "Gobernador"],
                    ["carrera" => "Director de cine"],
                    ["carrera" => "Realizador de televisión"],
                    ["carrera" => "Controlador aéreo"],
                ],
                "ENSEÑANZA" => [
                    ["carrera" => "Profesor de formación profesional"],
                    ["carrera" => "Profesor de Educación Física"],
                    ["carrera" => "Profesor de ciencias naturales"],
                    ["carrera" => "Profesor de Física"],
                    ["carrera" => "Profesor de Dibujo"],
                    ["carrera" => "Profesor de Música"],
                    ["carrera" => "Profesor de Idiomas"],
                    ["carrera" => "Profesor de Educación especial"],
                    ["carrera" => "Profesor de Primaria/Secundaria"],
                    ["carrera" => "Profesor de Historia"],
                    ["carrera" => "Profesor  de Lenguaje"],
                    ["carrera" => "Profesor de Lenguas clásicas"],
                    ["carrera" => "Profesor de Universidad"],
                    ["carrera" => "Profesor de Arte"],
                    ["carrera" => "Profesor de Matemáticas"],
                    ["carrera" => "Profesor de Química"],
                ],
                "RELACIONES ECONOMICAS Y EMPRESARIALES" => [
                    ["carrera" => "Cajero"],
                    ["carrera" => "Interventor de banco"],
                    ["carrera" => "Productor de cine"],
                    ["carrera" => "Director de banco"],
                    ["carrera" => "Auxiliar administrativo"],
                    ["carrera" => "Gerente"],
                    ["carrera" => "Director de ventas"],
                    ["carrera" => "Empleado de comercio"],
                    ["carrera" => "Secretario"],
                    ["carrera" => "Contable"],
                    ["carrera" => "Director de empresa"],
                    ["carrera" => "Economista"],
                    ["carrera" => "Técnico de marketing"],
                    ["carrera" => "Intendente comercial"],
                    ["carrera" => "Agente inmobiliaria"],
                    ["carrera" => "Jefe de ventas o compras"],
                ],
                "RELACIONES ECONOMICAS Y EMPRESARIALES" => [
                    ["carrera" => "Cajero"],
                    ["carrera" => "Interventor de banco"],
                    ["carrera" => "Productor de cine"],
                    ["carrera" => "Director de banco"],
                    ["carrera" => "Auxiliar administrativo"],
                    ["carrera" => "Gerente"],
                    ["carrera" => "Director de ventas"],
                    ["carrera" => "Empleado de comercio"],
                    ["carrera" => "Secretario"],
                    ["carrera" => "Contable"],
                    ["carrera" => "Director de empresa"],
                    ["carrera" => "Economista"],
                    ["carrera" => "Técnico de marketing"],
                    ["carrera" => "Intendente comercial"],
                    ["carrera" => "Agente inmobiliaria"],
                    ["carrera" => "Jefe de ventas o compras"],
                ],
                "COLERICO" => [
                    ["carrera" => "Abogado"],
                    ["carrera" => "Diplomativo"],
                    ["carrera" => "Orador"],
                    ["carrera" => "Periodista"],
                    ["carrera" => "Escritor"],
                    ["carrera" => "Politico"],
                    ["carrera" => "Medico"],
                    ["carrera" => "Ciencias Sociales"],
                    ["carrera" => "Personalidad: Alegre, buen humor, expansivo, habil, astuto, cofiado, voluble, impulsivo, violento, hablador, extrovertido, inteligencia rapido, cualidadesde de lider"],
                    ["carrera" => "Area: Sociales"],
                ],
                "APASIONADO" => [
                    ["carrera" => "Fisico - matematico"],
                    ["carrera" => "ciencias teoricas"],
                    ["carrera" => "Ingeneiro"],
                    ["carrera" => "Arquitecto"],
                    ["carrera" => "Constructor"],
                    ["carrera" => "Aplicado al trabajo, bueno para negocios, compasivo, sencillo, desconfiado, relfexivo,testarudo, impaciente gusta de deportes"],
                    ["carrera" => "Area: FIMA"],
                ],
                "NERVIOSO" => [
                    ["carrera" => "Carreras artisticas"],
                    ["carrera" => "Escritor"],
                    ["carrera" => "Periodista"],
                    ["carrera" => "Pintor"],
                    ["carrera" => "Decorador"],
                    ["carrera" => "Cine"],
                    ["carrera" => "Teatro"],
                    ["carrera" => "Impulsivo, violento, susceptible, coluble, inestable, inconstante, artista, tenso, bondadoso, generoso, servicial, surperficial."],
                    ["carrera" => "Area: Sociales - Arte"],
                ],
                "SENTIMENTAL" => [
                    ["carrera" => "Profesor"],
                    ["carrera" => "Medico"],
                    ["carrera" => "Psicologo"],
                    ["carrera" => "Escritor"],
                    ["carrera" => "Quimico"],
                    ["carrera" => "Biologo"],
                    ["carrera" => "Meditativo, preocupado, introvertido, ansioso, fragilidad, timido, desconfiado, sensible, susceptible, pasivo, soñador, honrado, bondadoso."],
                    ["carrera" => "Area: Quibio"],
                ],
                "SANGUINEO" => [
                    ["carrera" => "Medicina General"],
                    ["carrera" => "Abogado"],
                    ["carrera" => "Banquero"],
                    ["carrera" => "Periodista"],
                    ["carrera" => "Ingeniero agronomo"],
                    ["carrera" => "Literatura"],
                    ["carrera" => "Calmado, decidido, sereno, diplomatico, astuto, egosita, amigable, afable, servicial, misericordioso"],
                    ["carrera" => "Area: Quibio -Sociales"],
                ],
                "FLEMATICO" => [
                    ["carrera" => "Juez"],
                    ["carrera" => "Ingeneiro"],
                    ["carrera" => "Medico"],
                    ["carrera" => "Odontolgo"],
                    ["carrera" => "Banquero"],
                    ["carrera" => "Administrador contable"],
                    ["carrera" => "Contador"],
                    ["carrera" => "tolerante, coherente, puntual, minucioso, metodico, sincero, alegre, honrado, despreocupado, insensible, frio"],
                    ["carrera" => "Area: FIMA - Quibio - Comercio"],
                ],
                "AMORFO" => [
                    ["carrera" => "Farmaceutico"],
                    ["carrera" => "Quimico"],
                    ["carrera" => "Trabajos de oficina"],
                    ["carrera" => "Secretario"],
                    ["carrera" => "compasivo, tolerante, optimista, amable, sociable, valiente, pasible, impertubable, impuntual, perezosos, incumplido"],
                    ["carrera" => "Area: Secretario - Quibio"],
                ],
                "APATICO" => [
                    ["carrera" => "Farmaceutico"],
                    ["carrera" => "veterinario"],
                    ["carrera" => "odontologo"],
                    ["carrera" => "callado, calmado, conservador, interesado, introvertido, carece de entusiasmo"],
                    ["carrera" => "Area: Secretario - Quibio"],
                ],
            ];
            if ($value->id_prueba == 1) {
                $cipsa = $carreas[$value->resultado_area];
            } else {
                $pearson = $carreas[$value->resultado_area];
            }
        }
        return ['cipsa' => $cipsa, 'pearson' => $pearson];
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\models\Prueba  $prueba
     * @return \Illuminate\Http\Response
     */
    public function show(Prueba $prueba)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\models\Prueba  $prueba
     * @return \Illuminate\Http\Response
     */
    public function edit(Prueba $prueba)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\models\Prueba  $prueba
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prueba $prueba)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\models\Prueba  $prueba
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prueba $prueba)
    {
        //
    }
}
