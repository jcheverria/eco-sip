<?php

namespace App\Http\Controllers;

use App\Laravue\JsonResponse;
use App\Laravue\models\Alumno;
use App\Laravue\Models\Entrevista;
use App\Laravue\models\Resultado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EstudianteController extends Controller
{
    //
    public function getNotificaciones()
    {

        $tabla_notificacion = [];
        if (Auth::user()->can('do test pearson')) {
            array_push($tabla_notificacion, [
                'descripcion' => 'Tiene pendiente el test de pearson',
                'nota' => 'Tiene pendiente el test de pearson',
                'tipo' => 'pearson',
            ]);
        }
        if (Auth::user()->can('do test cipsa')) {
            array_push($tabla_notificacion, [
                'descripcion' => 'Tiene pendiente el test de cipsa',
                'nota' => 'Tiene pendiente el test de cipsa',
                'tipo' => 'cipsa',
            ]);
        }
        $alumno = Alumno::with(['user' => function ($query) {
            $query->where('id', Auth::user()->id);
        }])->get()->whereNotNull('user')->toArray();

        //var_dump($alumno);
        $id_alumno = 0;
        foreach ($alumno as $key => $value) {
            $id_alumno = $value['id'];
        }
        $entrevista_resultado[0] = [];
        $entrevista_resultado = Entrevista::where('id_alumno', $id_alumno)->get();
        if (count($entrevista_resultado) > 0) {
            if ($entrevista_resultado[0]->estado == 'ENTREVISTADO') {
                foreach ($entrevista_resultado as $key => $value) {
                    array_push($tabla_notificacion, [
                        'descripcion' => 'El resultado de la entrevista fue dado en la entrevista.',
                        'nota' => 'Debe aceptar el resultado dado en la entrevista',
                        'tipo' => 'notificacion'
                    ]);
                }
            } 
        } else {
            $entrevista_resultado[0] = [];
        }


        return ['tabla' => ($tabla_notificacion), 'id_alumno' => $id_alumno, 'entrevista' => $entrevista_resultado[0]];
    }

    public function getEntrevistaMatriz()
    {
        $alumnos_resultados = Alumno::with('resultado')->with('entrevista')->with('periodos')->get()->where('resultado', '!=', '[]')->toArray();
        $json_matriz = [];
        //$resultados = Resultado::with('periodo')->with('alumno')->get();
        foreach ($alumnos_resultados as $key => $value) {
            //dd($value['periodos']['periodo_letra']);
            $cipsa = '';
            $pearson = '';
            foreach ($value['resultado'] as $val) {

                if ($val['id_prueba'] == 1) {
                    $cipsa = $val['resultado_area'];
                }
                if ($val['id_prueba'] == 2) {
                    $pearson = $val['resultado_area'];
                }
            }
            array_push($json_matriz, [
                "periodo" => $value['periodos']['periodo_letra'],
                "codigo_estudiante" => $value['cod_alumno'],
                "apeliidos_nombres" => $value['primer_nombre'] . ' ' . $value['segundo_nombre'] . ' ' . $value['primer_apellido'] . ' ' . $value['segundo_apellido'],
                "cipsa" => $cipsa,
                "pearson" => $pearson,
                "observaciones" => $value['entrevista'][0]['observacion'],
                "fecha_entrevista" =>  $value['entrevista'][0]['fecha_inicial_agendada'],
                "seleccion_carrera" =>  $value['entrevista'][0]['carrera_seleccionada'],
                "universdad" =>  $value['entrevista'][0]['universidad']
            ]);
        }

        //return $json_matriz;
        return response()->json(new JsonResponse(['items' => $json_matriz, 'total' => 1]));
    }
}
