<?php

namespace App\Http\Controllers;

use App\Laravue\JsonResponse;
use App\Laravue\models\Alumno;
use App\Laravue\Models\Entrevista;
use App\Laravue\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class EntrevistaController extends Controller
{
    //
    public function getEntrevista()
    {
        $entrevistas = Entrevista::with(['periodo' => function ($query) {
            $query->where('estado', 'ACTIVO');
        }])->with('alumno')->get();
        //dd($entrevistas);
        return response()->json(new JsonResponse(['items' => $entrevistas, 'total' => 1]));
    }
    public function getFiltroEntrevista(Request $request)
    {
        $nombre = strtoupper ($request->name);
        $entrevistas = [];
        if (strlen($request->name) > 0) {
            $query_entrevistas = Entrevista::with(['periodo' => function ($query) {
                $query->where('estado', 'ACTIVO');
            }])->with(['alumno' => function ($query) use ($nombre) {
                $query->where('nombre_completo', 'like', "%$nombre%");
            }])->get()->whereNotNull('alumno');
            foreach ($query_entrevistas as $key => $value) {
                array_push($entrevistas, $value);
            }
        } else {
            $entrevistas = Entrevista::with(['periodo' => function ($query) {
                $query->where('estado', 'ACTIVO');
            }])->with('alumno')->get();
        }

        if (strlen($request->sort) > 0 && $request->sort != 'Todos') {
            $entrevistas = Entrevista::with(['periodo' => function ($query) {
                $query->where('estado', 'ACTIVO');
            }])->with('alumno')->where('estado', $request->sort)->get();
        }
        return response()->json(new JsonResponse(['items' => $entrevistas, 'total' => 3]));
    }
    public function agendarEntrevista(Request $request)
    {
        $entrevistas = Entrevista::find($request->id_entrevista);
        $entrevistas->fecha_inicial_agendada = $request->fecha_inicio;
        $entrevistas->fecha_final_agendada = $request->fecha_fin;
        $entrevistas->estado = 'AGENDADO';
        $entrevistas->save();
        $alumno = Alumno::find($entrevistas->id_alumno);
        $nombre = $alumno->primer_nombre . ' ' . $alumno->primer_apellido . ' ' . $alumno->segundo_apellido;
        $fecha =  $entrevistas->fecha_inicial_agendada;
            
        if (env('ACTIVAR_CORREOS')) {
            /* $emails = $alumno->email; */
        } else {
            $emails = ["jorgecheverria96@gmail.com", "jcheverria96@gmail.com"];
        }
        // Armar correo y pasarle datos para el constructor
        $correo = new \App\Mail\AgendaEntrevista(['nombre' => $nombre, 'fecha' => $fecha]);
        # ¡Enviarlo!
        Mail::to($emails)->send($correo);
        return 'ok';
    }
    public function guardarEntrevista(Request $request)
    {
        $user = User::find($request->id_user);
        $user->revokePermissionTo('do test cipsa');
        $user->revokePermissionTo('do test pearson');
        $entrevistas = Entrevista::find($request->id_entrevista);
        $entrevistas->observacion = $request->observacion;
        $entrevistas->carrera_seleccionada = $request->carrera_seleccionada;
        $entrevistas->universidad = $request->universidad;
        $entrevistas->estado = 'ENTREVISTADO';
        $entrevistas->save();
        $user = User::find($request->id_user);
        return 'ok';
    }
    public function guardarConfirmarEntrevista(Request $request)
    {
        //dd($request);
        $entrevistas = Entrevista::find($request->id_entrevista);
        $entrevistas->confirmacion = 'CONFIRMADO';
        $entrevistas->save();
        return 'ok';
    }
    public function getEntrevistaCalendario(Request $request)
    {
        $entrevistas = Entrevista::with(['periodo' => function ($query) {
            $query->where('estado', 'ACTIVO');
        }])->with('alumno')->get();
        $array_calendario = [];
        foreach ($entrevistas as $key => $value) {
            if ($value->fecha_inicial_agendada != null) {
                array_push($array_calendario, [
                    'title' => 'Entrevista con' . ' ' . $value->alumno->primer_nombre . ' ' . $value->alumno->primer_apellido . ' ' . $value->alumno->segundo_apellido,
                    'start' => $value->fecha_inicial_agendada,
                    'end' => $value->fecha_final_agendada,
                ]);
            }
        }
        return response()->json(new JsonResponse(['items' => $array_calendario, 'total' => 1]));
        return $array_calendario;
    }
}
