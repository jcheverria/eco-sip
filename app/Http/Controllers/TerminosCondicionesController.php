<?php

namespace App\Http\Controllers;

use App\Laravue\models\Resultado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TerminosCondicionesController extends Controller
{
    //
    public function terminos_condiciones()
    {
        $usuario = Auth::user();
        //dd($usuario->isAdmin());
        if ($usuario->can('do test pearson')) {
            $prueba = Resultado::where('id_alumno', $usuario->id)->get();
            if (count($prueba) > 0) {
                $resultado = ['tipo' => 1, 'descripcion' => 'Ya tiene un test'];
            } else {
                $resultado = ['tipo' => 2, 'descripcion' => 'terminos y condiciones'];
            }
            return $resultado;
        }
        if ($usuario->can('do test cipsa')) {
            $prueba = Resultado::where('id_alumno', $usuario->id)->get();
            if (count($prueba) > 0) {
                $resultado = ['tipo' => 1, 'descripcion' => 'Ya tiene un test'];
            } else {
                $resultado = ['tipo' => 2, 'descripcion' => 'terminos y condiciones'];
            }
            return $resultado;
        }
        return;
        if ($usuario->isAdmin()) {
            echo 'es admin';
        } else {
            if ($usuario->can('do test pearson') && $usuario->can('do test cipsa')) {
                $prueba = Prueba::where('id_alumno', $usuario->id);
                if ($prueba) {
                    $resultado = ['tipo' => 1, 'descripcion' => 'Ya tiene un test'];
                } else {
                    $resultado = ['tipo' => 2, 'descripcion' => 'terminos y condiciones'];
                }
                return $resultado;
            }
        }
    }
}
