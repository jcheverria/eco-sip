<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePeriodo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'periodo_letra' => 'required|unique:periodos|max:255',
        ];
    }
    public function messages()
    {
        return [
            'periodo_letra.unique' => 'El periodo está repetido',
        ];
    }
}
