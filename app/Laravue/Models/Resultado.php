<?php

namespace App\Laravue\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
class Resultado extends Model
{
    //
    protected $fillable = [
        'id_periodo', 'id_prueba', 'id_alumno', 'resultado_json', 'id_usuario_creacion', 'resultado_area', 'resultado_id_area'
    ];
    const CREATED_AT = 'creacion';
    const UPDATED_AT = 'actualizacion';

    public function periodo(): BelongsTo
    {
        return $this->belongsTo(Periodo::class, 'id_periodo');
    }
    public function alumno(): BelongsTo
    {
        return $this->belongsTo(Alumno::class, 'id_alumno');
    }
}
