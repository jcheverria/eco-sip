<?php

namespace App\Laravue\models;

use App\Laravue\Models\Entrevista;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Alumno extends Model
{
    //
    const CREATED_AT = 'creacion';
    const UPDATED_AT = 'actualizacion';
    protected $table = 'alumnos';
    protected $fillable = [
        'id_user',
        'primer_nombre',
        'segundo_nombre',
        'primer_apellido',
        'segundo_apellido',
        'nombre_completo',
        'fecha_nacimiento',
        'des_curso',
        'paralelo',
        'email',
        'cod_alumno',
        'estado',
        'id_periodo',
        'id_usuario_creacion',
        'id_usuario_actualizacion',

    ];
    public function creador(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id_usuario_creacion');
    }
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id_user');
    }
    public function periodos(): BelongsTo
    {
        return $this->belongsTo(Periodo::class, 'id_periodo');
    }
    public function editor(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id_usuario_actualizacion');
    }
    public function resultado()
    {
        return $this->hasMany(Resultado::class, 'id_alumno');
    }
    public function entrevista()
    {
        return $this->hasMany(Entrevista::class, 'id_alumno');
    }
}
