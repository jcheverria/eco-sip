<?php

namespace App\Laravue\models;

use Illuminate\Database\Eloquent\Model;

class Periodo extends Model
{
    //
    const CREATED_AT = 'creacion';
    const UPDATED_AT = 'actualizacion';
    protected $table = 'periodos';
    protected $fillable = [
        'id',
        'periodo_letra',
        'jornada',
        'estado',
        'id_usuario_creacion',
        'id_usuario_actualizacion',
        'creacion',
        'actualizacion',

    ];
    public function creador(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id_usuario_creacion');
    }
    public function editor(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id_usuario_actualizacion');
    }
}
