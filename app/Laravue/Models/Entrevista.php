<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Entrevista extends Model
{
    //
    const CREATED_AT = 'creacion';
    const UPDATED_AT = 'actualizacion';
    protected $table = 'entrevistas';
    protected $fillable = [
        'id',
        'id_alumno',
        'id_periodo',
        'estado',
        'id_usuario_creacion',
        'id_usuario_actualizacion',
        'creacion',
        'actualizacion',
        'observacion',
        'carrera_seleccionada',
        'universidad',

    ];

    public function creador(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id_usuario_creacion');
    }
    public function editor(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id_usuario_actualizacion');
    }
    public function periodo(): BelongsTo
    {
        return $this->belongsTo(Periodo::class, 'id_periodo');
    }
    public function alumno(): BelongsTo
    {
        return $this->belongsTo(Alumno::class, 'id_alumno');
    }
}
