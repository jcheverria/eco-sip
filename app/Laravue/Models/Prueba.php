<?php

namespace App\Laravue\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Prueba extends Model
{
    protected $table = 'pruebas';
    protected $fillable = [
        'descripcion',
        'formato',
        'estado',
        'id_usuario_creacion',
        'id_usuario_actualizacion',
        'creacion',
        'actualizacion',
        
    ];
    public function creador(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id_usuario_creacion');
    }
    public function editor(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id_usuario_actualizacion');
    }
}
