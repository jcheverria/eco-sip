import pandas as pd
from keras.models import load_model
import sys
model = load_model('D:/datos/laravel/eco-sip/storage/modelos/modelo.h5')

""" ejemplo de argumento que se debe pasar "60,129,33,46,41,59,56,86,58,78,48,87" """

lista = sys.argv[1]
lista = tuple(map(int, lista.split(',')))
predicion = model.predict_classes([lista])

print(predicion.astype(int))